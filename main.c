#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>

//HAUTEUR ET LARGEUR DU PLATEAU
#define HAUTEUR 10
#define LARGEUR 10

char grille[HAUTEUR][LARGEUR];
//x,y position de P
int x=0,y=0;
//nombre de case bonus
int case_bonus=3;

//fonction de creation d'un tableau qui sera le plateau de jeux
void create_plan(){
    // rempli le tableau grille par le caractère '-'
    memset(grille,'-',LARGEUR*HAUTEUR);

}


//fonction d'affichage du plateau de jeux
void print_grille(){
    for (int i=0;i<HAUTEUR;i++){
        for (int j=0;j<LARGEUR;j++){
            printf("%c",grille[i][j]);
        }
        printf("\n");

    }
}

//fonction pour déplacer le personnage
void moving(char touche){
    //on stocke la position avant le mouvement dans des variables temporaires pour pouvoir la récupérer facilement en cas de sortie de plateau
    int last_x=x;
    int last_y=y;
    grille[y][x]='-';
    switch (touche){
        case 'z':
            y--;
            break;
        case 's':
            y++;
            break;
        case 'q':
            x--;
            break;
        case 'd': x++;
            break;
    }
    if (x >= 0 && x < LARGEUR && y >= 0 && y < HAUTEUR){
        if (grille[y][x]=='X'){
            printf("bravo vous avez win 1 point\n");
            case_bonus--;
        }
    }
    else {
        printf("il n’est pas possible d’aller par ici, veuillez saisir a nouveau votre déplacement\n");
        //le personnage reprend donc sa position initiale
        x=last_x;
        y=last_y;
    }
    //nouvelle position du personnage :
    grille[y][x]='P';


}





int main(){
    create_plan();
    //on définit la position initiale
    grille[y][x]='P';
    //maintenant celle des cases bonus
    grille[1][0]='X';
    grille[0][1]='X';
    grille[0][8]='X';
    print_grille();
//grille affichée et initialisé maintenant mouvement du perso
    char move;
    printf("vous avez 5 lancés de dés\n");
    printf("choisir le déplacement (z,q,s,d) puis enter\n");
    for (int i=1;i<=5;i++){
        srand(time(NULL));
        int dice=1+rand()%6;
        printf("%deme lancé, votre dé a fait %d\n",i,dice);
        for (int j=1;j<=dice;j++){
            printf("%d mouvement\n",j);
            move=getchar();
            //comme mouvement réalisé avec clavier + enter et on ne veut pas prendre le saut de ligne en compte :
            while (getchar()!='\n');
            moving(move);
            print_grille();
            if (case_bonus==0){
                printf("vous avez gagné bravo, vous pouvez continuer à vous déplacer pour le principe\n");
            }
        }
    }
    printf("merci d'avoir jouer.\n");
}

